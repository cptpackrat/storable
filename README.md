# @nfi/storable
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

A typed wrapper for reading/writing JSON-serialisable data.

## Installation
```
npm install @nfi/storable
```

## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/storable).

[npm-image]: https://img.shields.io/npm/v/@nfi/storable.svg
[npm-url]: https://www.npmjs.com/package/@nfi/storable
[pipeline-image]: https://gitlab.com/cptpackrat/storable/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/storable/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/storable/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/storable/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
