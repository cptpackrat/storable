import { readFile, writeFile } from 'fs/promises'
import { readFileSync, writeFileSync } from 'fs'

/* eslint-disable-next-line @typescript-eslint/consistent-type-definitions */
type SerialisableObject = { [index: string]: SerialisableProperty }
type SerialisableProperty = SerialisableObject | SerialisableProperty[] | string | number | boolean | null | undefined

/** Limitations for data that can safely be used with Storable. */
export type StorableData = SerialisableObject & {
  storeSync?: never
  store?: never
  proxy?: never
}

/** Limitations for data that can safely be used with StorableProxy. */
export type ProxiableData = SerialisableObject & {
  storeSync?: never
  store?: never
}

/** A wrapper for reading/writing JSON-serialisable data. */
export type Storable<T extends StorableData> = T & {
  /** Write Storable to file after optionally applying changes.
    * @param data Data changes to apply. */
  storeSync: (data?: Partial<T>) => void
  /** Write Storable to file after optionally applying changes.
    * @param data Data changes to apply. */
  store: (data?: Partial<T>) => Promise<void>
  /** Return a proxied view of a child object within this Storable.
    * @param data Child object to proxy. */
  proxy: <T extends ProxiableData>(data: T) => StorableProxy<T>
}

/** A proxied view of a child object within a Storable. */
export type StorableProxy<T extends ProxiableData> = T & {
  /** Write the parent Storable to file after optionally applying changes to the proxied object.
    * @param data Changes to apply. */
  storeSync: (data?: Partial<T>) => void
  /** Write the parent Storable to file after optionally applying changes to the proxied object.
    * @param data Changes to apply. */
  store: (data?: Partial<T>) => Promise<void>
}

/** Load JSON data from file, and return it as a Storable. Optionally accepts default
  * data that will be returned as a Storable rather than throwing ENOENT if the given
  * path does not exist.
  * @param path Path to load.
  * @param data Default data. */
export function loadSync<T extends StorableData> (path: string, data?: T): Storable<T> {
  try {
    return populate(path, JSON.parse(readFileSync(path, 'utf8')))
  } catch (err) {
    if (data !== undefined && err.code === 'ENOENT') {
      return populate(path, Object.assign({}, data))
    }
    throw err
  }
}

/** Load JSON data from file, and return it as a Storable. Optionally accepts default
  * data that will be returned as a Storable rather than throwing ENOENT if the given
  * path does not exist.
  * @param path Path to load.
  * @param data Default data. */
export async function load<T extends StorableData> (path: string, data?: T): Promise<Storable<T>> {
  try {
    return populate(path, JSON.parse(await readFile(path, 'utf8')))
  } catch (err) {
    if (data !== undefined && err.code === 'ENOENT') {
      return populate(path, Object.assign({}, data))
    }
    throw err
  }
}

/** Return a new Storable associated with, but not loaded from, the specified file.
  * @param path Path to load.
  * @param data Initial data. */
export function create<T extends StorableData> (path: string, data: T): Storable<T> {
  return populate(path, Object.assign({}, data))
}

/** Install Storable methods into an object. */
function populate<T extends StorableData> (path: string, data: T): Storable<T> {
  let prChain = Promise.resolve()
  return Object.defineProperties(data, {
    storeSync: {
      enumerable: false,
      writable: false,
      value: (apply?: Partial<T>): void => {
        writeFileSync(path, JSON.stringify(Object.assign(data, apply), null, 2))
      }
    },
    store: {
      enumerable: false,
      writable: false,
      value: async (apply?: Partial<T>): Promise<void> => {
        prChain = prChain
          .catch(() => {})
          .then(async () => await writeFile(path, JSON.stringify(Object.assign(data, apply), null, 2)))
        return await prChain
      }
    },
    proxy: {
      enumerable: false,
      writeable: false,
      value: <T extends StorableData>(part: T): StorableProxy<T> => {
        const methods = {
          storeSync: (apply?: Partial<T>) => {
            Object.assign(part, apply)
            writeFileSync(path, JSON.stringify(data, null, 2))
          },
          store: async (apply?: Partial<T>) => {
            prChain = prChain
              .catch(() => {})
              .then(async () => {
                Object.assign(part, apply)
                await writeFile(path, JSON.stringify(data, null, 2))
              })
            return await prChain
          }
        }
        return new Proxy(part, {
          get: (obj: any, key) => methods[key as keyof typeof methods] ?? obj[key]
        })
      }
    }
  })
}
