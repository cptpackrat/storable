import { join } from 'path'
import { tmpdir } from 'os'
import { load, loadSync, create } from '../src'
import { mkdtempSync, rmdirSync, writeFileSync, readFileSync } from 'fs'

let next = 0
const dir = mkdtempSync(join(tmpdir(), 'storable-'))
afterAll(() => {
  rmdirSync(dir, { recursive: true })
})

const loadData = { foo: 'foo', bar: 'bar' }
const loadDefaults = { foo: 'default', bar: 'default' }
const loadPath = writeJSON(nextPath(), loadData)
const loadPathBad = loadPath + '.nope'
const loadPropDesc = {
  configurable: false,
  enumerable: false,
  writable: false,
  value: expect.any(Function)
}

const storeBefore = { foo: 'foo', bar: 'bar' }
const storePatch = { foo: 'patched' }
const storeAfter = { foo: 'patched', bar: 'bar' }

const proxyDataOuter = { foo: 'foo', bar: { boo: 'boo' } }
const proxyDataInner = { boo: 'boo' }
const proxyDataPatch = { boo: 'patched' }
const proxyDataAfter = { foo: 'foo', bar: { boo: 'patched' } }

describe('Storable', () => {
  describe('load', () => {
    it('reads data from file', async () => {
      expect(await load(loadPath)).toStrictEqual(loadData)
      expect(await load(loadPath, loadDefaults)).toStrictEqual(loadData)
    })
    it('returns defaults for non-existent file', async () => {
      expect(await load(loadPathBad, loadDefaults)).toStrictEqual(loadDefaults)
    })
    it('returns defaults as a shallow copy', async () => {
      const a = await load(loadPathBad, loadDefaults)
      const b = await load(loadPathBad, loadDefaults)
      expect(a === b).toBe(false)
      expect(a === loadDefaults).toBe(false)
      expect(b === loadDefaults).toBe(false)
    })
    it('rejects non-existent file with no defaults', async () => {
      await expect(load(loadPathBad)).rejects.toThrow(/ENOENT/)
    })
    it('adds methods as non-enumerable properties', async () => {
      const a = await load(loadPath)
      const b = await load(loadPathBad, loadDefaults)
      expect(Object.getOwnPropertyDescriptor(a, 'storeSync')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(a, 'store')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(a, 'proxy')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'storeSync')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'store')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'proxy')).toMatchObject(loadPropDesc)
    })
  })
  describe('loadSync', () => {
    it('reads data from file', () => {
      expect(loadSync(loadPath)).toStrictEqual(loadData)
      expect(loadSync(loadPath, loadDefaults)).toStrictEqual(loadData)
    })
    it('returns defaults for non-existent file', () => {
      expect(loadSync(loadPathBad, loadDefaults)).toStrictEqual(loadDefaults)
    })
    it('returns defaults as a shallow copy', () => {
      const a = loadSync(loadPathBad, loadDefaults)
      const b = loadSync(loadPathBad, loadDefaults)
      expect(a === b).toBe(false)
      expect(a === loadDefaults).toBe(false)
      expect(b === loadDefaults).toBe(false)
    })
    it('rejects non-existent file with no defaults', () => {
      expect(() => loadSync(loadPathBad)).toThrow(/ENOENT/)
    })
    it('adds methods as non-enumerable properties', () => {
      const a = loadSync(loadPath)
      const b = loadSync(loadPathBad, loadDefaults)
      expect(Object.getOwnPropertyDescriptor(a, 'storeSync')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(a, 'store')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(a, 'proxy')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'storeSync')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'store')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(b, 'proxy')).toMatchObject(loadPropDesc)
    })
  })
  describe('create', () => {
    it('returns a new storable', () => {
      expect(create(loadPath, loadDefaults)).toStrictEqual(loadDefaults)
    })
    it('returns data as a shallow copy', () => {
      const a = create(loadPath, loadDefaults)
      const b = create(loadPath, loadDefaults)
      expect(a === b).toBe(false)
      expect(a === loadDefaults).toBe(false)
      expect(b === loadDefaults).toBe(false)
    })
    it('adds methods as non-enumerable properties', () => {
      const data = create(loadPath, loadDefaults)
      expect(Object.getOwnPropertyDescriptor(data, 'storeSync')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(data, 'store')).toMatchObject(loadPropDesc)
      expect(Object.getOwnPropertyDescriptor(data, 'proxy')).toMatchObject(loadPropDesc)
    })
  })
  describe('store', () => {
    it('writes data to file', async () => {
      const path = nextPath()
      const data = create(path, storeBefore)
      await data.store()
      expect(readJSON(path)).toStrictEqual(data)
      expect(readJSON(path)).toStrictEqual(storeBefore)
    })
    it('writes patched data to file', async () => {
      const path = nextPath()
      const data = create(path, storeBefore)
      await data.store(storePatch)
      expect(readJSON(path)).toStrictEqual(data)
      expect(readJSON(path)).toStrictEqual(storeAfter)
    })
  })
  describe('storeSync', () => {
    it('writes data to file', () => {
      const path = nextPath()
      const data = create(path, storeBefore)
      data.storeSync()
      expect(readJSON(path)).toStrictEqual(data)
      expect(readJSON(path)).toStrictEqual(storeBefore)
    })
    it('writes patched data to file', () => {
      const path = nextPath()
      const data = create(path, storeBefore)
      data.storeSync(storePatch)
      expect(readJSON(path)).toStrictEqual(data)
      expect(readJSON(path)).toStrictEqual(storeAfter)
    })
  })
  describe('proxy', () => {
    it('returns a proxied view of some data', () => {
      const a = create(nextPath(), proxyDataOuter)
      const b = a.proxy(a.bar)
      expect(b === a.bar).toBe(false)
      expect(b).toStrictEqual(a.bar)
      expect(b).toStrictEqual(proxyDataInner)
      b.boo = 'baz'
      expect(a.bar.boo).toBe('baz')
    })
    it('adds methods via proxied get', () => {
      const a = create(nextPath(), proxyDataOuter)
      const b = a.proxy(a.bar)
      expect('store' in b).toBe(false)
      expect('storeSync' in b).toBe(false)
      expect('store' in a.bar).toBe(false)
      expect('storeSync' in a.bar).toBe(false)
      expect(b.store).toBeInstanceOf(Function)
      expect(b.storeSync).toBeInstanceOf(Function)
    })
    describe('store', () => {
      it('writes data to file', async () => {
        const path = nextPath()
        const data = create(path, proxyDataOuter)
        await data.proxy(data.bar).store()
        expect(data).toStrictEqual(proxyDataOuter)
        expect(readJSON(path)).toStrictEqual(proxyDataOuter)
      })
      it('writes patched data to file', async () => {
        const path = nextPath()
        const data = create(path, proxyDataOuter)
        await data.proxy(data.bar).store(proxyDataPatch)
        expect(data).toStrictEqual(proxyDataAfter)
        expect(readJSON(path)).toStrictEqual(proxyDataAfter)
      })
    })
    describe('storeSync', () => {
      it('writes data to file', () => {
        const path = nextPath()
        const data = create(path, proxyDataOuter)
        data.proxy(data.bar).storeSync()
        expect(data).toStrictEqual(proxyDataOuter)
        expect(readJSON(path)).toStrictEqual(proxyDataOuter)
      })
      it('writes patched data to file', () => {
        const path = nextPath()
        const data = create(path, proxyDataOuter)
        data.proxy(data.bar).storeSync(proxyDataPatch)
        expect(data).toStrictEqual(proxyDataAfter)
        expect(readJSON(path)).toStrictEqual(proxyDataAfter)
      })
    })
  })
})

function nextPath (): string {
  return join(dir, `${next++}.json`)
}

function writeJSON (path: string, data: any): string {
  writeFileSync(path, JSON.stringify(data))
  return path
}

function readJSON (path: string): any {
  return JSON.parse(readFileSync(path, 'utf8'))
}
