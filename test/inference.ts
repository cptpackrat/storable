/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/consistent-type-assertions */
/* eslint-disable @typescript-eslint/consistent-type-definitions */

import { create, ProxiableData, Storable, StorableProxy } from '../src'

type StorableEmpty = Storable<{}>

type StorableFlat = Storable<{
  a: string
  b: number
  c: boolean
  d: null
  e: undefined
  f: {}
  g: []
}>

type StorableNested = Storable<{
  obj: {
    a: string
    b: number
    c: boolean
    d: null
    e: undefined
    f: {}
    g: []
  }
}>

type StorableArray = Storable<{
  a: string[]
  b: number[]
  c: boolean[]
  d: null[]
  e: undefined[]
  f: Array<{}>
  g: Array<[]>
}>

/* @ts-expect-error */
type UnstorableString = Storable<string>

/* @ts-expect-error */
type UnstorableNumber = Storable<number>

/* @ts-expect-error */
type UnstorableBoolean = Storable<boolean>

/* @ts-expect-error */
type UnstorableArray = Storable<[]>

/* @ts-expect-error */
type UnstorableSet = Storable<Set<string>>

/* @ts-expect-error */
type UnstorableNestedSet = Storable<{ set: Set<string> }>

/* @ts-expect-error */
type UnstorableBuffer = Storable<Buffer>

/* @ts-expect-error */
type UnstorableNestedBuffer = Storable<{ buffer: Buffer }>

/* @ts-expect-error */
type UnstorablePropertyStoreSync = Storable<{ storeSync: string }>

/* @ts-expect-error */
type UnstorablePropertyStore = Storable<{ store: string }>

/* @ts-expect-error */
type UnstorablePropertyProxy = Storable<{ proxy: string }>

/* @ts-expect-error */
type UnproxiablePropertyStoreSync = StorableProxy<{ storeSync: string }>

/* @ts-expect-error */
type UnproxiablePropertyStore = StorableProxy<{ store: string }>

type Testable = {
  a: string
  b: number
  c: boolean
}

describe('Storable<T>', () => {
  it('infers types correctly', () => {
    const test = create<Testable>('test.json', {
      a: 'foo',
      b: 42,
      c: true
    })
    assertType<string>()(test.a, true)
    assertType<number>()(test.b, true)
    assertType<boolean>()(test.c, true)
    assertType<(data?: Partial<Testable>) => Promise<void>>()(test.store, true)
    assertType<(data?: Partial<Testable>) => void>()(test.storeSync, true)
    assertType<<T extends ProxiableData>(data: T) => StorableProxy<T>>()(test.proxy, true)
  })
})

describe('StorableProxy<T>', () => {
  it('infers types correctly', () => {
    const test = create('test.json', {}).proxy<Testable>({
      a: 'foo',
      b: 42,
      c: true
    })
    assertType<string>()(test.a, true)
    assertType<number>()(test.b, true)
    assertType<boolean>()(test.c, true)
    assertType<(data?: Partial<Testable>) => Promise<void>>()(test.store, true)
    assertType<(data?: Partial<Testable>) => void>()(test.storeSync, true)
  })
})

function assertType<T> () {
  return <V>(val: V, exp: [T, V] extends [V, T] ? true : false) => {}
}
